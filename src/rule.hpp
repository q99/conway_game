#ifndef CONWAY_GAME_RULE_HPP
#define CONWAY_GAME_RULE_HPP

#include <functional>

namespace Game
{
    using Rule = std::function<bool(bool is_alive, size_t alive_count)>;

    const Rule base_rule = [](bool is_alive, size_t alive_count) -> bool
    {
        if (is_alive)
            return alive_count == 2 || alive_count == 3;

        return alive_count == 3;
    };
}

#endif //CONWAY_GAME_RULE_HPP
