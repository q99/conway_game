#include <utility>
#include <sstream>


#include "game.hpp"

template <class T>
inline void hash_combine(std::size_t& seed, const T& v)
{
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
}

namespace Game
{
    void to_next_state(Space::ISpace::Ptr & space, const Rule & rule)
    {
        std::unordered_map<Point, bool> difference;
        for (const auto & point: space->get_alive())
        {
            size_t alive_count = 0;
            space->for_each_neighbour(point,
                                      [&alive_count, &difference, &rule, &space](const Point & pnt, bool is_alive) -> void
                                      {
                                          if (is_alive)
                                          {
                                              ++alive_count;
                                          }
                                          else
                                          {
                                              if (difference.find(pnt) == difference.end())
                                                  difference[pnt] = rule(false, get_alive_neighbour(space, pnt));
                                          }
                                      });

            difference[point] = rule(true, alive_count);
        }

        for (const auto & [point, status]: difference)
            space->set_status(point, status);
    }

    GameHandler::GameHandler(Space::ISpace::Ptr & space, Rule rule): _space(space), _rule(std::move(rule)) { }

    Space::ISpace::Ptr GameHandler::get()
    {
        return _space;
    }

    bool GameHandler::next_state()
    {
        Game::to_next_state(_space, _rule);

        std::set<size_t> hash;

        {
            std::hash<Point> hasher;
            const auto alive = _space->get_alive();
            for (const Point & pnt: alive)
                hash.insert(hasher(pnt));

            if (hash.size() != alive.size())
            {
                std::stringstream error;
                error <<"Hash collision: ";

                for (const auto & [x,y]: alive)
                    error << x << "," << y << ';';

                throw std::logic_error(error.str());
            }
        }

        if (hash.empty()) // All dead
            return true;

        auto it = hash.begin();
        size_t seed = std::hash<size_t>()(*it);

        while (++it != hash.end())
            hash_combine(seed, *it);

        if (_hash.find(seed) != _hash.end())
            return true;

        _hash.insert(seed);
        return false;
    }
}
