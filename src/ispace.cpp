#include "ispace.hpp"

namespace Space
{
    size_t get_alive_neighbour(ISpace::Ptr & space, const Point & point)
    {
        size_t count = 0;
        space->for_each_neighbour(point,
                                  [&count](const Point & pnt, bool is_alive)
                                  {
                                      count += is_alive ? 1 : 0;
                                  });

        return count;
    }
}
