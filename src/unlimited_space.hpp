#ifndef CONWAY_GAME_UNLIMITED_SPACE_HPP
#define CONWAY_GAME_UNLIMITED_SPACE_HPP

#include <unordered_set>
#include <algorithm>

#include "ispace.hpp"

namespace Space
{
    class UnlimitedSpace: public ISpace
    {
    public:
        void for_each_neighbour(const Point & point, const ProcessPoint & processPoint) const override;

        bool get_status(const Point & point) const override;

        void set_status(const Point & point, bool is_alive) override;

        const std::list<Point> get_alive() const override;

    private:
        std::unordered_set<Point> _alive;
    };
}

#endif //CONWAY_GAME_UNLIMITED_SPACE_HPP
