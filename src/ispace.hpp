#ifndef CONWAY_GAME_ISPACE_HPP
#define CONWAY_GAME_ISPACE_HPP

#include <list>
#include <tuple>
#include <array>
#include <memory>
#include <functional>

#include "point.hpp"

namespace Space
{
    using Neighbour = std::array<std::pair<Point, bool>, 8>;

    struct ISpace
    {
        using ProcessPoint = std::function<void(const Point &, bool)>;
        using Ptr = std::shared_ptr<ISpace>;

        virtual void for_each_neighbour(const Point & point, const ProcessPoint &) const = 0;

        virtual bool get_status(const Point & point) const = 0;

        virtual void set_status(const Point & point, bool is_alive) = 0;

        virtual const std::list<Point> get_alive() const = 0;

    };

    size_t get_alive_neighbour(ISpace::Ptr & space, const Point & point);
}

#endif //CONWAY_GAME_ISPACE_HPP
