#ifndef CONWAY_GAME_POINT_HPP
#define CONWAY_GAME_POINT_HPP

#include <tuple>

namespace Space
{
    using Point = std::pair<int, int>;

    inline std::array<Point, 8> get_neighbourhood_points(const Point & point)
    {
        const auto & x = point.first, & y = point.second;
        return {
                Point{x, y - 1},
                Point{x, y + 1},

                Point{x - 1, y},
                Point{x - 1, y + 1},
                Point{x - 1, y - 1},

                Point{x + 1, y},
                Point{x + 1, y + 1},
                Point{x + 1, y - 1}
        };
    }
}

template <>
class std::hash<Space::Point>
{
public:
    // ToDo Check hash function
    size_t operator()(const Space::Point & point) const
    {
        std::hash<int> hasher;
        const size_t hash = hasher(point.first);

        return hasher(point.second) + 0x9e3779b9 + (hash << 6) + (hash >> 2);
    }
};

#endif //CONWAY_GAME_POINT_HPP
