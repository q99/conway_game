//
// Created by sad on 20.05.19.
//
#include "unlimited_space.hpp"

namespace Space
{
    bool UnlimitedSpace::get_status(const Point & point) const
    {
        return _alive.find(point) != _alive.end();
    }

    void UnlimitedSpace::set_status(const Point & point, bool is_alive)
    {
        if (is_alive)
        {
            _alive.insert(point);
            return;
        }

        if (auto target = _alive.find(point); target != _alive.end())
        {
            _alive.erase(target);
        }
    }

    const std::list<Point> UnlimitedSpace::get_alive() const
    {
        return std::list<Point>(_alive.begin(), _alive.end());
    }

    void UnlimitedSpace::for_each_neighbour(const Point & point, const ISpace::ProcessPoint & processPoint) const
    {
        const auto neighbour_points = get_neighbourhood_points(point);

        std::for_each(neighbour_points.begin(), neighbour_points.end(),
                [&processPoint, this](const Point & point)
                {
                    processPoint(point, get_status(point));
                });
    }
}
