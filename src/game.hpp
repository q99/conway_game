#ifndef CONWAY_GAME_GAME_HPP
#define CONWAY_GAME_GAME_HPP

#include <set>
#include <unordered_map>

#include "ispace.hpp"
#include "rule.hpp"

namespace Game
{
    using Space::Point;

    void to_next_state(Space::ISpace::Ptr & space, const Rule & rule = base_rule);

    class GameHandler
    {
    public:
        GameHandler(Space::ISpace::Ptr & space, Rule rule);

        Space::ISpace::Ptr get();

        // ToDo Add test for next state with end rules
        bool next_state();

    private:
        Rule _rule;
        Space::ISpace::Ptr _space;
        std::set<size_t> _hash;
    };
}

#endif //CONWAY_GAME_GAME_HPP
