#define CATCH_CONFIG_MAIN

#include "../src/game.hpp"
#include "../src/rule.hpp"
#include "../src/ispace.hpp"
#include "../include/catch.hpp"
#include "../src/unlimited_space.hpp"

template < typename Iterator >
Space::ISpace::Ptr initialize(Iterator begin, Iterator end)
{
    auto space = std::dynamic_pointer_cast<Space::ISpace>(std::make_shared<Space::UnlimitedSpace>());

    for (auto pnt = begin; pnt != end; ++pnt)
        space->set_status(*pnt, true);

    return space;
}

TEST_CASE("Game")
{
    using namespace Space;
    using namespace Game;

    SECTION("Beacon")
    {
        const std::array<Space::Point, 6> start_state = {{{0, 2}, {0, 3}, {1, 3}, {2, 0}, {3, 0}, {3, 1}}};
        auto space = initialize(start_state.begin(), start_state.end());

        to_next_state(space, base_rule);

        REQUIRE(space->get_alive().size() == 8);

        for (const auto & pnt: start_state)
        {
            REQUIRE(space->get_status(pnt));
        }

        REQUIRE(space->get_status({1, 2}));
        REQUIRE(space->get_status({2, 1}));

        to_next_state(space, base_rule);

        REQUIRE(space->get_alive().size() == 6);

        for (const auto & pnt: start_state)
        {
            REQUIRE(space->get_status(pnt));
        }
    }

    SECTION("Block")
    {
        const std::array<Point, 4> start_state = {{{0, 0}, {0, 1}, {1, 0}, {1, 1}}};
        auto space = initialize(start_state.begin(), start_state.end());

        to_next_state(space, base_rule);

        REQUIRE(space->get_alive().size() == 4);

        for (const auto & pnt: start_state)
        {
            REQUIRE(space->get_status(pnt));
        }
    }

    SECTION("Glider")
    {
        const std::array<Point, 5> start_state = {{{0, 0}, {1, 0}, {2, 0}, {2, 1}, {1, 2}}};
        auto space = initialize(start_state.begin(), start_state.end());

        to_next_state(space);

        const std::array<Point, 5> first_state = {{{0, 1}, {1, 0}, {1, -1}, {2, 1}, {2, 0}}};

        REQUIRE(space->get_alive().size() == first_state.size());

        for (const auto & pnt: first_state)
        {
            REQUIRE(space->get_status(pnt));
        }

        to_next_state(space);

        const std::array<Point, 5> second_state = {{{0, 0}, {1, -1}, {2, -1}, {2, 0}, {2, 1}}};

        REQUIRE(space->get_alive().size() == second_state.size());

        for (const auto & pnt: second_state)
        {
            REQUIRE(space->get_status(pnt));
        }
    }
}
